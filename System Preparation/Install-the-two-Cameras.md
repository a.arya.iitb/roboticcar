# Install the ROS Nodes for the cameras &  The keyboard teleop

## Description
In this step we will install ROS nodes needed to operate the cameras.

## Steps

- Install the ROS2 video for linux ros package that will be used to stream the videos from the two cameras (this step is done on the Board).
```shell
sudo apt-get install ros-foxy-v4l2-camera
```

- Install the keyboard teleop node on your host computer.
```shell
sudo apt install ros-foxy-teleop-twist-keyboard
```
