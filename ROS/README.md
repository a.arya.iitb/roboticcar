# Robot Software Packages

## Description

In this section we will use ROS2 Foxy to build the main software that controls the robot and run this software.

## Steps

- Building the ROS node. 
```shell
ssh pi  # ssh to your pi board connected to thw wifi network
source /opt/ros/foxy/setup.bash  # Source the ROS OS
mkdir -p ~/ros2_ws/src  # Make the workspace folder
cd ~/ros2_ws/src
git clone https://gitlab.com/roboticcar/roboticcar.git
cd ~/ros2_ws
colcon build --packages-select robotic_car  --symlink-install  # Build the ros node
```

- Running the node (you will need to run the node from root, because RPi.GPIO needs to run with root permissions to control the board).
```shell
sudo -i  # Enter root shell in interactive mode
source /opt/ros/foxy/setup.bash  # Source the ROS OS
cd /home/YOUR_USER_NAME/ros2_ws  # change directory to the ros2 ws folder you created in the previous step
source install/setup.bash  # Source the workspace script
ros2 launch robotic_car robot_launch.py
```

- In another terminal, Run the keyboard teleop to control your robot (on the host computer)
```shell
source /opt/ros/foxy/setup.bash  # Source the ROS OS
ros2 run teleop_twist_keyboard teleop_twist_keyboard
```
- You can control your robot using:
  - i -> Forward.
  - j -> Rotate Left.
  - l -> Rotate Right.
  - k -> Stop.

- To view the left sensor speed sensor values, open a new terminal (on the host computer).
```shell
source /opt/ros/foxy/setup.bash  # Source the ROS OS
ros2 topic echo /left_sensor
```
  Values are in rotation per second, the values doesn't reflect the rotation directions.

- To view the right sensor speed sensor values, open a new terminal (on the host computer).
```shell
source /opt/ros/foxy/setup.bash  # Source the ROS OS
ros2 topic echo /right_sensor
```
  Values are in rotation per second, the values doesn't reflect the rotation directions.

- To view the camera streams, open a new terminal on the host computer.
```shell
source /opt/ros/foxy/setup.bash  # Source the ROS OS
ros2 run rqt_image_view rqt_image_view /left_image_raw &  # The Left camera stream 
ros2 run rqt_image_view rqt_image_view /right_image_raw & # The right camera stream
```

**Note**

- If you don't see either of the camera streams, make that the right video devices are selected in [robot_launch.py](robotic_car/launch/robot_launch.py) at the camera nodes.
