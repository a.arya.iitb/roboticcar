from setuptools import setup

package_name = 'robotic_car'

setup(
    name=package_name,
    version='1.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        # Adding the main launch file to the package installation folder
        ('share/' + package_name + '/launch/', ['launch/robot_launch.py']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='remon',
    maintainer_email='remoncomputer@gmail.com',
    description='This is the main ROS package that drives the Robotic Car',
    license='MIT Licence',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            # The robot driver node script
            'my_robot_driver = robotic_car.my_robot_driver:main',
            # The sensor node script
            'sensor_node = robotic_car.sensor_node:main',
        ],
    },
)
