# Fixing the upper chassic and the wheels

## Description

This file describes the the fixing of the upper floor of the robotic car chassic and the fixing of the wheels.


## Tools

- The upper floor of the chassic, after fixing its electronics.

- 6 M3 * 7.5 screws.
    ![12 M3 * 7.5 screws](imgs/6_M3_7_5_screws.jpg)

- 4 Robotic wheels.

- screwdriver.
    ![screwdriver](imgs/screwdriver.jpg)


## Steps

1. Add the upper floor.

2. Fix the first screw in a column from above.
    ![fixing a screw from above](imgs/fixing_a_screw_from_above.jpg)

3. fix the remaining screws from above.

4. Fix the wheels in the motors.
    ![fixing the wheels in the motors](imgs/fixing_the_wheels.jpg)