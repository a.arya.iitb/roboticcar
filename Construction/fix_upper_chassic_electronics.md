# Fixing the Upper Chassic Electronics

## Description

We will fix the upper chassic electronics.

## Tools

- upper car chassic. 

- 2 x (screw + nut).

- 1 x 18650 battery holder.

- 2 x speed sensors.

- small knife.

- hot glue.

- Raspberri Pi Board.

- 4 x short cable ties.

- 2 x webcam cameras.

- 4 x long cable ties.

- Stove

## Steps

1. Use the two screws and the two bolts to fix the Battery Holder at the back of the car, keep the face of the battery holder on the upper side of the chassic.

![Battery holder fixing upper side](imgs/Battery%20Holder%20fixing%20upper%20side.jpg)

![Battery holder fixing lower side](imgs/Battery%20Holder%20fixing%20lower%20side.jpg)

2. Heat the knife on the stove and cut a clearance for the speed sensors, you can cut through the acrylic using the heated knife, cut the clearance on backend of the car.

![Speed Sensors Clearance](imgs/clearance_for_speed_sensors.jpg)

3. Fix the two speed sensors on their backs using the hot glue on their backs.

![Speed Sensor](imgs/speed_sensor.jpg)

![One Speed Sensor Fixed](imgs/one_speed_sensor_fixed.jpg)

![Both Speed Sensors Fixed](imgs/both_speed_sensors_fixed.jpg)

4. Use the 4 short cable ties to tie the board to the upper side of the chassic, keep the connectors side beside the speed sensors.

![Pi Board fixed](imgs/fixing%20pi%20board.jpg)

5. Fix the two webcam cameras using the 4 long cable ties.

![Camera Fixed Frontal View](imgs/Camera-fixed-1.jpg)

![Camera Fixed Upper View](imgs/Camera-fixed-2.jpg)

![Camera Fixed Back View](imgs/Camera-fixed-3.jpg)

![Camera Fixed Right View](imgs/Camera-fixed-4.jpg)

![Camera Fixed Left View](imgs/Camera-fixed-5.jpg)
