# Fixing the motors

## Description

In this step we will fix the motors on the lower aclyric floor of the Robotic Car & we will also fix the columns that connect the lower chassic with the upper one.

## Tools

- Lower Acrylic Chassic Floor.
    ![Acrylic Chassic Floor](imgs/lower_acrylic_car_floor.jpg)

- 4 Motors.
    ![4 Motors](imgs/4_motors.jpg)

- Wire Stripper, It is advised that you use a more comfortable wire stripper.

- 4 Coded Discs.
    ![4 Coded Discs](imgs/4_coded_disc.jpg)

- 8 Acrylic T-Shaped Fastners.
    ![8 Acrylic T-Shaped Fastners](imgs/acrylic_fastners.jpg)

- 8 M3*28 Screws.
    ![M3 x 28 screws](imgs/M3x28_screws.jpg)

- 8 M3 Nuts.
    ![8 M3 Nuts](imgs/M3_Nuts.jpg)

- 6 M3-30 Columns.
    ![6 M3-30 columns](imgs/6_M3-30_columns.jpg)

- screwdriver.
    ![screwdriver](imgs/screwdriver.jpg)

- 6 M3 * 7.5 screws.
    ![12 M3 * 7.5 screws](imgs/6_M3_7_5_screws.jpg)

# Steps

1. Place the 4 encoded discs on the 4 motors.
    ![Encoded discs placed on the motors](imgs/place_enocoded_discs.jpg)

2. Place the first motor on the robotic car lower floor.
    ![first motor on lower floor](imgs/first_motor_on_lower_floor.jpg)

3. Place 2 T-Fastners as showing by the next two images.
    ![placing t_fastner 1](imgs/placing_t_fastner_1.jpg)
    ![placing t_fastner 2](imgs/placing_t_fastner_2.jpg)

4. Fix the Motor with two M3 * 28 Screws, like this:
    ![Fixing with M3 Screw](imgs/fixing_with_screws.jpg)

5. Tie one nut on each screw.
    ![Nuts Tied](imgs/tied_nuts.jpg)

6. Ensure that the coded disc has a safe distance with the acrylic floor.
    ![coded disc with safe distance](imgs/coded_disc_safe_distance.jpg)

7. You should have the motor fixed, like this:
    ![one motor fuly fixed](imgs/one_motor_fuly_fixed.jpg)

8. Repeat Step 3 to 8 For the remaining three motors, you should end with this:
    ![all motors fixed](imgs/all_motors_fixed.jpg)

9. Tie the wire terminal of every two motors on the same side of the car, tie the positive wires together and the negative wires together.
    ![tying wire terminal](imgs/tying_wire_terminal.jpg)

10. Fix the screw in the column through the lower floor.
    ![Screw fixed in column from down side](imgs/screw_fixed_in_column.jpg)
    ![Screw fixed in column from upper side](imgs/screw_fixed_in_column_from_above.jpg)

11. Fix the Remaining columns. 
    ![All columns fixed](imgs/all_columns_fixed.jpg)